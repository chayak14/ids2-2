/* ########################################################################################################################
This script processes and integrates all the commands that we have developed so far in our Ecological Footprint Analytics course.
The relevant tables are created, the raw soy-oil-trade data and list of Comtrade-countries is uploaded and modified (if necessary) in my schema, and a view of per-capita-value of exports of soya oil is generated. */

# Lines 6,9,11 and 12 are commented out as these are nonSQL server commands that don’t work in Workbench but included here because they are necessary to run scripts in Command Line 
#TEE C:\\ids2\w3_script.out


#WARNINGS;

#Set @start_time=UNIX_Timestamp();
#Select NOW() as `Data Load Started`;

## Determine the Schema to Use ##
use `_ids2_chaya`;  #to output the follow revised tables and views 

## Create countries table Version 2 ##
# DROP TABLE IF EXISTS `_ids2_chaya`.`countries_v2`;

CREATE TABLE `countries_v2` (
    `code` SMALLINT(3) NOT NULL,
    `country_name_full` VARCHAR(100) NOT NULL,
    `country_name_abbrev` VARCHAR(100) NOT NULL,
    `country_comments` VARCHAR(100) DEFAULT NULL,
    `iso2` CHAR(3) DEFAULT NULL,					# some records have N/A which is 3chars
    `iso3` CHAR(3) DEFAULT NULL,
    `start_year` SMALLINT(4) NOT NULL,
    `end_year_text` CHAR(4) NOT NULL,				# since this is text
    PRIMARY KEY (`code`),							# i will describe this in session2
    UNIQUE (`country_name_full`)					# i will describe this in session2
);

LOAD DATA LOCAL INFILE "C:\\ids2\\CountryCodes.csv" 
	INTO TABLE `countries_v2` 
	FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' 
	LINES TERMINATED BY '\r\n' 
	IGNORE 1 LINES;

## Add another column to contain the integer value of end year ##
ALTER TABLE `countries_v2` ADD `end_year` SMALLINT(4);

## Put sensible data in that column in just one step ##
UPDATE `countries_v2` SET `end_year` = nullif(`end_year_text`, 'Now');

## Create country_pop_2015 table Version 2 ##
# DROP TABLE IF EXISTS `_ids2_chaya`.`country_pop_2015_v2`;

CREATE TABLE `country_pop_2015_v2` (
    `country_code` CHAR(3) NOT NULL,
    `country_name` VARCHAR(100) NOT NULL,
	`popin2015` BIGINT DEFAULT NULL,
    PRIMARY KEY (`country_code`)
);

LOAD DATA LOCAL INFILE "C:\\ids2\\WorldBankPop.csv" 
	INTO TABLE `country_pop_2015_v2` 
	FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' 
	LINES TERMINATED BY '\r\n' 
	IGNORE 1 LINES
	(`country_name`, `country_code`, @pop)
    SET
    `popin2015` = nullif(@pop, '')
	;

## Create soya_oil_trade table Version 2 ##
# DROP TABLE IF EXISTS `_ids2_chaya`.`soya_oil_trade_v2`;

CREATE TABLE `soya_oil_trade_v2` (
  `year` SMALLINT(4) NOT NULL,
  `trade_flow` VARCHAR(10) DEFAULT NULL,
  `reporter` SMALLINT(5) DEFAULT NULL,
  `commodity_code` VARCHAR(5) DEFAULT NULL,				#store as text since 012 different from 12
  `commodity_description` VARCHAR(254) DEFAULT NULL,
  `trade_quantity_units` VARCHAR(254) DEFAULT NULL,
  `trade_quantity` BIGINT DEFAULT NULL,					#allow for very big numbers!
  `netweight_kg` BIGINT DEFAULT NULL,					#allow for very big numbers!
  `trade_value` BIGINT DEFAULT NULL					#allow for very big numbers!
);

LOAD DATA LOCAL INFILE "C:\\ids2\\soya_oil_trade.csv" 
	INTO TABLE `soya_oil_trade_v2` 
	FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' 
	LINES TERMINATED BY '\r\n' 
	IGNORE 1 LINES;

## Create a view of per-capita-value of exports of soya oil ##
CREATE VIEW `_ids2_chaya`. `views_from_the_6ix_v2` AS

# all countries with pop data and which also reported exports...
#
#SELECT popcountries.*, tradecountries.*, tradeflows.*
SELECT popcountries.country_name, (tradeflows.netweight_kg/popcountries.popin2015) as `Exported kg per capita`
FROM `_ids2_chaya`.`country_pop_2015_v2` as popcountries
LEFT JOIN `_ids2_chaya`.`countries_v2` AS tradecountries
	ON tradecountries.iso3 = popcountries.country_code
LEFT JOIN `_ids2_chaya`.`soya_oil_trade_v2` as tradeflows
	ON tradeflows.reporter = tradecountries.code
WHERE tradeflows.trade_flow = 'Export'
ORDER BY `Exported kg per capita` DESC;  
#returns 106 rows

# Lines 103,104 and 106 are commented out as these are nonSQL server commands that don’t work in Workbench but included here because they are necessary to run scripts in Command Line 
#Select NOW() as `Data Load Ended`\p;
#Select ((UNIX_TIMESTAMP() - @start_time)/60) as `Total elapsed time in minutes`\p;

#NOTEE
